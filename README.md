# RKE2

Ansible role for installing an [RKE2](https://docs.rke2.io/) Kubernetes Cluster with HA managed by [kube-vip](https://kube-vip.io/).

## Role Variables

| Variable                | Required | Default        | Choices                                                                          | Comments                                 |
|-------------------------|----------|----------------|----------------------------------------------------------------------------------|------------------------------------------|
| rke2_version            | no       | v1.25.6+rke2r1 | See [Github releases](https://github.com/rancher/rke2/releases) for version list | RKE2 version deployed on node |
| rke2_ha_interface       | no       | eth0           |                                                                                  | Network interface used for HA IP on RKE2 nodes |
| rke2_ha_ip              | yes      |                | Available IP (must be in the same network as nodes)                              | IP announced with ARP used for control plane HA and Load Balancer |
| rke2_ha_dnat_ip         | no       |                | Public IP DNATed to rke2_ha_ip                                                   | Public IP DNATed to rke2_ha_ip used for control plane HA and Load Balancer |
| rke2_token              | yes      |                |                                                                                  | Token used for RKE2 node authentification |
| rke2_tls_san            | no       | empty list     | DNS alt name of your Kubernetes cluster, for using DNS instead of IP             | Additionnals SAN name for selfsigned RKE2 certificate |
| rke2_tcp_services       | no       | empty dict     | TCP services dict (see Ingress Nginx doc)                                        | https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/ |
| rke2_udp_services       | no       | empty dict     | UDP services dict (see Ingress Nginx doc)                                        | https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/ |
| rke2_letsencrypt_email  | no       |                | Email address                                                                    | Email used by [cert-manager](https://cert-manager.io/) for Let's Encrypt account |
| rke2_elastic_host       | no       |                | Elasticsearch HTTPS URL                                                          | Elasticsearch HTTPS url endpoints for Elastic Agent output |
| rke2_elastic_token      | no       |                |                                                                                  | Token with write access to Elasticsearch data streams used by Elastic Agent |

## Example Playbook

~~~
---
- hosts: all
  user: root
  vars:
    rke2_ha_ip: "192.168.2.250"
    rke2_ha_dnat_ip: "198.51.100.30"
    rke2_token: "keEodOM3dFenYEsvjiyRXqoevs53jy4P"
    rke2_tls_san:
      - k8s-lb.example.com
    rke2_letsencrypt_email: "security@example.com"
  roles:
     - ansible-role-rke2
~~~

## Misc

### TLS certificates for mTLS

A selfsigned CA is automatically created by cert-manager, for creating a certificate for an external service which will use this CA, eg. for a PostgreSQL server:

~~~
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: postgresql0
  namespace: default
spec:
  commonName: "192.168.X.XXX"
  secretName: postgresql0-tls
  privateKey:
    algorithm: ECDSA
    size: 256
  issuerRef:
    name: selfsigned-ca
    kind: ClusterIssuer
    group: cert-manager.io
~~~

After that, you can retrieve the CA certificate, the server certificate and key with:

~~~
kubectl -n default get secret postgresql0-tls -o yaml
~~~

## Testing

A vagrant configuration file is available for testing purpose.

To test the deployment on 3 servers :

~~~
vagrant up
~~~

To redeploy after a modification :

~~~
vagrant provision
~~~

To SSH into a VM :

~~~
vagrant ssh k8s01
~~~

To delete the previously created VMs :

~~~
vagrant down
~~~

## Author

This Ansible module is maintained by [Leni.tech](https://www.leni.tech/).

## License

This Ansible module is MIT Licensed. See [LICENSE]() for full details.
